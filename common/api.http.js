const http=uni.$u.http;

export function getGoodByNameNo(){
	
	
	uni.showLoading({
		title:"正在加载..."
	})
	uni.$u.http.post('/wxm/good/getGoodByNameNo',{}).then(res=>{
		console.log(res);
		uni.hideLoading();
	}).catch(err=>{
		uni.$u.toast(err);
		uni.hideLoading();
	});
}
