import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

const state={
	token:undefined
}
const mutations={
	increment(state){
		state.count++;
	},
	setToken(state,token){
		console.log("set token in store",token);
		state.token=token;
	},
	clearToken(state){
		state.token=undefined;
	}
}
const getters={
	
	getToken(){
		return state.token;
	}
}
export default new Vuex.Store({
	state,
	mutations,
	getters
});