const http=uni.$u.http;

export function searchGoodByNo(queryParams){
	return http.post('/wxm/good/getGoodByNameNo',queryParams);
}


export function loadProvider(){
	
	return http.post('/wxm/provider/listProvider',{});
}

export function wxPageListGood(params){
	return http.post('/wxm/good/wxPageListGood',params);
}

export function saveGoodCheckIn(data){
	return http.post('/wxm/good/saveCheckIn',data);
}

export function saveGoodCheckOut(data){
	return http.post('/wxm/good/saveCheckOut',data);
}

export function listGoodWithIndex(){
	return http.post('/wxm/good/listGoodWithIndex',{});
}

export function loadById(id){
	return http.post('/wxm/good/loadById',{id:id});
}