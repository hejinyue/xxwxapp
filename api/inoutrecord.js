const http=uni.$u.http;

export function pageListMineCheck(queryParams){
	return http.post('/wxm/inOutRecord/pageListInOutRecord',queryParams);
}

export function pageListAllCheck(queryParams){
	return http.post('/wxm/inOutRecord/pageListListAllInOutRecord',queryParams);
}

export function selectInOutRecordById(id){
	return http.post('/wxm/inOutRecord/selectById',{inOutRecordId:id});
}

export function correctInOut(data){
	return http.post('/wxm/inOutRecord/correctInOut',data);
}

export function getPersonCheckOutStatis(params){
	return http.post('/wxm/inOutRecord/getPersonCheckOutStatis',params)
}