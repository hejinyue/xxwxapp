const http=uni.$u.http;

export function regist(registForm){
	return http.post('/wxm/auth/regist',registForm);
}

export function login(loginForm){
	return http.post('/wxm/auth/login',loginForm); 
}

export function handleLoginStorage(wxAuthRes){
	uni.setStorageSync("token",wxAuthRes.loginToken);
	uni.setStorageSync("userId",wxAuthRes.userId);
	uni.setStorageSync("userNickName",wxAuthRes.userNickName);
	uni.setStorageSync("userPhone",wxAuthRes.userPhone);
	uni.setStorageSync("distributeType",wxAuthRes.distributeType)
	uni.setStorageSync("authPrivilDto",JSON.stringify(wxAuthRes.authPrivilDto))
}

export function readLoginStorage(){
	let token=uni.getStorageSync("token");
	if(!token){
		return undefined;
	}
	let userId=uni.getStorageSync("userId");
	let userNickName=uni.getStorageSync("userNickName");
	let userPhone=uni.getStorageSync("userPhone");
	let distributeType=uni.getStorageSync("distributeType");
	let authPrivilDto=JSON.parse(uni.getStorageSync("authPrivilDto"));
	let wxAuthRes={
		"token":token,
		"userId":userId,
		"userNickName":userNickName,
		"userPhone":userPhone,
		"distributeType":distributeType,
		"authPrivilDto":authPrivilDto
	};
	return wxAuthRes;
}

export function logout(){
	return http.post('/wxm/auth/logout',{});
}

export function clearLogin(){
	uni.removeStorageSync("token");
	uni.removeStorageSync("userId");
	uni.removeStorageSync("userNickName");
	uni.removeStorageSync("userPhone");
	uni.removeStorageSync("authList");
	uni.removeStorageSync("distributeType");
	
}

export function getToken(){
	let token=uni.getStorageSync("token");
	return token;
}