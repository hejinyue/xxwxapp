const http=uni.$u.http;

export function loadLastTwentyNotice(params){
	return http.post('/wxm/notice/pageListNotice',params);
}

export function getByNoticeId(params){
	return http.post('/wxm/notice/getByNoticeId',params);
}